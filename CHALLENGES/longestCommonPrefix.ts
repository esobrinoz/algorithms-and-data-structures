/**  LONGEST COMMON PREFIX
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

 

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
 

Constraints:

1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lower-case English letters.
*/


function longestCommonPrefix(strs: string[]): string {
    const length: number = strs.length;
    if (length < 1 || length >200) return "";
    let commonPrefix: string = strs[0];
    let commonPrefixLength: number = commonPrefix.length;
    if (commonPrefixLength < 1 || commonPrefixLength> 200) return "";
    else if(length === 1) return commonPrefix;
    let i: number = 0;
    let charAux: string;
    let nextString: string;
    let newCommonPrefix: string ="";
    while(i < length-1) {
        charAux = strs[i];
        nextString = strs[i+1];
        commonPrefixLength = commonPrefix.length;
        newCommonPrefix = "";
        for(let j=0;j< commonPrefixLength;j++) {
            if (charAux.charAt(j) === nextString.charAt(j)) 
                newCommonPrefix += charAux.charAt(j);
            else break;
        }
        if(!newCommonPrefix) return "";
        commonPrefix = newCommonPrefix;
        i++;
    }
    return commonPrefix;
};
 /*  COMPLEXITY ANALYSIS: 
        - Time complexity : O((S)) => Number of the sum oh characters of each string
        - Space complexity : O(W) => Being W the number of the characters of the longest word of the array of strings
*       - Runtime: 88ms
        - Memory: 41.3 MB
*/

//Follow up: Could you solve it using recursivity?
function longestCommonPrefix2(strs: string[]): string {
    const length: number = strs.length;
    if (length < 1 || length >200) return "";
    const longPrefix: string = strs[0];
    return recur(longPrefix, 1, strs);
};

function recur(longestPrefix: string, index: number, strs: string[]) {
    if(index === strs.length) return longestPrefix;
    let commonPrefixLength: number = longestPrefix.length;
    if (commonPrefixLength < 1 || commonPrefixLength> 200) return "";
    let i: number = 0;
    let charAux: string;
    let newCommonPrefix: string ="";
    for(let i=0;i< commonPrefixLength;i++) {
        if (longestPrefix.charAt(i) === strs[index].charAt(i)) 
            newCommonPrefix += longestPrefix.charAt(i);
        else break;
    }
    if (!longestPrefix) return "";
    return recur(newCommonPrefix , index+1, strs);
}

 /*  COMPLEXITY ANALYSIS: 
        - Time complexity : O(\log_{10}(n))O(log10(n)). We divided the input by 10 for every iteration, 
        so the time complexity is O(\log_{10}(n))O(log 10(n))

        - Space complexity : O(n) => being n the number of recursive calls (not taking into account 
            the number of characters of the longest string of the array of strings)
        - Runtime: 92ms
        - Memory: 41.8 MB
*      
*/