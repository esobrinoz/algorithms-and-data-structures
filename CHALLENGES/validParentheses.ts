/*    VALID PARENTHESES

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
 

Example 1:

Input: s = "()"
Output: true
Example 2:

Input: s = "()[]{}"
Output: true
Example 3:

Input: s = "(]"
Output: false
Example 4:

Input: s = "([)]"
Output: false
Example 5:

Input: s = "{[]}"
Output: true
 

Constraints:

1 <= s.length <= 104
s consists of parentheses only '()[]{}'.

*/

function isValid(s: string): boolean {
    const hash: Map<string, string> = new Map<string, string>();
    hash.set('(',')');
    hash.set('{','}');
    hash.set('[',']');
  
    const stack: string[] = [];
  
    for (const char of s) {
      if (hash.has(char)) stack.push(char);
      else {
        const top = stack.pop();
        if (top === undefined || hash.get(top) !== char) {
          return false;
        }
      }
    }
  
    return !stack.length;
  }

  /*  COMPLEXITY ANALYSIS: 
        - Time complexity : O((N)) => Number of the sum oh characters of the string
        - Space complexity : O(N) => Number of the sum oh characters of the string
*       - Runtime: 80ms
        - Memory: 40.8 MB
*/
